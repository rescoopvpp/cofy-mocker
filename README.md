# cofy-mocker

Script mimicking a COFY box in a real-life environment, replaying measurements from a data file. Use for testing the data pipeline.