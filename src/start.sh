#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    python cofymocker.py
else
    echo "CoFyMocker Block not enabled by environment variable"
fi