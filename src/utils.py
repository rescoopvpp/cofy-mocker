from typing import Union


def to_number(numeric_string: str) -> Union[int, float]:
    """
    Takes a string, converts it to an int if possible, else a float if possible,
    else raises a value error if neither is possible.
    """
    try:
        return int(numeric_string)
    except ValueError:
        try:
            return float(numeric_string)
        except ValueError:
            raise ValueError(numeric_string + " cannot be parsed as an int or a float.")
